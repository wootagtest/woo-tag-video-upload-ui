# RUN locally

git clone git clone https://manju16832003@bitbucket.org/wootagtest/woo-tag-video-upload-ui.git

`cd woo-tag-video-upload-ui`

`npm install`

`yarn start` OR `npm start`

Go to http://localhost:3000

Uploaded videos are located at `woo-tag-video-upload-service/videos`

# Commands

`yarn start`

`yarn build`

`yarn test`

`yarn eject`

## UI

https://reactstrap.github.io/

## Play Video

https://video-react.js.org/
